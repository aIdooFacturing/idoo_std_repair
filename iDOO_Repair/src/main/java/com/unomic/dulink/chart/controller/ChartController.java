package com.unomic.dulink.chart.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.unomic.dulink.chart.domain.BarChartVo;
import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.DataVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.chart.domain.UserVO;
import com.unomic.dulink.chart.service.ChartService;
import com.unomic.dulink.common.domain.CommonFunction;
/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService; 
	 
	
	@RequestMapping(value="request_Repair")
	public String request_repair() throws Exception{
		
		return "chart/request_repair";
	}
	
	@RequestMapping(value="RepairHst")
	public String RepairHst() throws Exception{
		
		return "chart/RepairHst";
	}
	
	
	@RequestMapping(value = "getRepairHst")
	@ResponseBody
	public String getRepairHst(ChartVo chartVo,HttpServletRequest request) {
		String str = null;
		try {
			
			
			str = chartService.getRepairHst(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "addRequestRepair")
	@ResponseBody
	public String addRequestRepair(ChartVo chartVo) {
		String str = null;
		try {
			str = chartService.addRequestRepair(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "getDeviceList4Report")
	@ResponseBody
	public String getDeviceList4Report(ChartVo chartVo) {
		String str = null;
		try {
			str = chartService.getDeviceList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	
	
	@RequestMapping(value="login")
	@ResponseBody 
	public String loginCnt(ChartVo chartVo){
		String str = "failed"; 
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="drawCircle")
	@ResponseBody 
	public String drawCircle(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.drawCircle(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="transferOprCnt")
	@ResponseBody 
	public String transferOprCnt(String val){
		String str = ""; 
		try {
			str = chartService.transferOprCnt(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="excel")
	@ResponseBody
	public void excel(){
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");
         
        Object[][] bookData = {
                {"Head First Java", "Kathy Serria", 79},
                {"Effective Java", "Joshua Bloch", 36},
                {"Clean Code", "Robert martin", 42},
                {"Thinking in Java", "Bruce Eckel", 35},
        };
 
        int rowCount = 0;
         
        for (Object[] aBook : bookData) {
            Row row = sheet.createRow(++rowCount);
             
            int columnCount = 0;
             
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
             
        }
         
        try (FileOutputStream outputStream = new FileOutputStream("/Users/jeongwan/Desktop/JavaBooks.xlsx")) {
            workbook.write(outputStream);
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getOprStockInfo")
	@ResponseBody 
	public String getOprStockInfo(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.getOprStockInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getReleaseInfoHist")
	@ResponseBody 
	public String getReleaseInfoHist(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.getReleaseInfoHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="transferOpr")
	public String transferOpr(){
		return "chart/transferOpr";
	};
	
	@RequestMapping(value="returnStock")
	public String returnStock(){
		return "chart/returnStock";
	};
	
	@RequestMapping(value="searchWorker")
	@ResponseBody 
	public String searchWorker(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.searchWorker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="getReleaseInfo")
	@ResponseBody 
	public String getReleaseInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getReleaseInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getLotInfoHistory")
	@ResponseBody 
	public String getLotInfoHistory(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLotInfoHistory(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getStockStatus")
	@ResponseBody 
	public String getStockStatus(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStockStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addCmplStockHistory")
	@ResponseBody 
	public String addCmplStockHistory(String val){
		String str = "";
		try {
			str = chartService.addCmplStockHistory(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addPrdCmplCnt")
	@ResponseBody 
	public String addPrdCmplCnt(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addPrdCmpl(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addPrdData")
	@ResponseBody 
	public String addPrdData(String val){
		String str = "";
		try {
			str = chartService.addPrdData(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getComList")
	@ResponseBody 
	public String getComList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getComList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getComName")
	@ResponseBody 
	public String getComName(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getOprNoList")
	@ResponseBody 
	public String getOprNoList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getOprNoList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getOprNo")
	@ResponseBody 
	public String getOprNo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getOprNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="showPopup")
	@ResponseBody
	public String showPopup(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.showPopup(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getPrdData")
	@ResponseBody
	public String getPrdData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getPrdData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="setCalcTy")
	@ResponseBody
	public String setCalcTy(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.setCalcTy(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getCheckList")
	@ResponseBody
	public String getCheckList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getCheckList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addCheckStandardList")
	@ResponseBody
	public String addCheckStandardList(String val){
		String str = "";
		try {
			str = chartService.addCheckStandardList(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="delChkStandard")
	@ResponseBody
	public String delChkStandard(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.delChkStandard(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getJSGroupCode")
	@ResponseBody
	public String getJSGroupCode(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getJSGroupCode(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getChkStandardList")
	@ResponseBody
	public String getChkStandrdList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getChkStandardList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addCheckStandard")
	@ResponseBody
	public String addCheckStandard(String val){
		String str = "";
		try {
			str = chartService.addCheckStandard(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="saveRow")
	@ResponseBody
	public String saveRow(String val){
		String str = "";
		try {
			str = chartService.saveRow(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="setPrgSet")
	@ResponseBody
	public String setPrgSet(String val){
		String str = "";
		try {
			str = chartService.setPrgSet(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getProgInfo")
	@ResponseBody
	public String getProgInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getProgInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="setSpdLoad")
	@ResponseBody
	public String setSpdLoad(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.setSpdLoad(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getCalcTy")
	@ResponseBody
	public ChartVo getCalcTy(ChartVo chartVo){
		try {
			chartVo = chartService.getCalcTy(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value="getSpdLoadInfo")
	@ResponseBody
	public ChartVo getSpdLoadInfo(ChartVo chartVo){
		try {
			chartVo = chartService.getSpdLoadInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value="getToolInfo")
	@ResponseBody
	public String getToolInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getToolInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;	
	}
	
	@RequestMapping(value="saveUpdatedStock")
	@ResponseBody
	public String saveUpdatedStock(String val){
		String str = "";
		try {
			str = chartService.saveUpdatedStock(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getOprNmList")
	@ResponseBody
	public String getOprNmList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getOprNmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getLotInfo")
	@ResponseBody
	public String getLotInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLotInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="testChart")
	public String test2(){
		return "chart/getTimeDataChart";
	};
	
	@RequestMapping(value="getShipLotNo")
	@ResponseBody
	public String getShipLotNo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getShipLotNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getOutLotTracer")
	@ResponseBody
	public String getOutLotTracer(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getOutLotTracer(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="chkLogin")
	@ResponseBody
	public String chkLogin(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.chkLogin(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="getLotNo")
	@ResponseBody
	public String getLotNo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLotNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getLotNoByPrdNo")
	@ResponseBody
	public String getLotNoByPrdNo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLotNoByPrdNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getDvcListByPrdNo")
	@ResponseBody
	public String getDvcListByPrdNo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getDvcListByPrdNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getCheckType")
	@ResponseBody
	public String getCheckType(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getCheckType(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getNewMatInfo")
	@ResponseBody
	public ChartVo getNewMatInfo(ChartVo chartVo){
		try {
			chartVo = chartService.getNewMatInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		return chartVo; 
	} 
	
	@RequestMapping(value="addNoOperation")
	public String addNoOperation(ChartVo chartVo){
		return "chart/addNoOperation";
	};
	
	@RequestMapping(value="addNoOperationHistory")
	public String addNoOperationHistory(ChartVo chartVo){
		return "chart/addNoOperationHistory";
	};
	
	@RequestMapping(value="getNonOpData")
	@ResponseBody
	public ChartVo getNonOpData(ChartVo chartVo){
		try {
			chartVo = chartService.getNonOpData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	@RequestMapping(value="updateNonOp")
	@ResponseBody
	public String updateNonOp(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.updateNonOp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addNonOp")
	@ResponseBody
	public String addNonOp(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addNonOp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="okDelNonOp")
	@ResponseBody
	public String okDelNonOp(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.okDelNonOp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getOprNm")
	@ResponseBody
	public String getOprNm(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getOprNm(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getNonOpTy")
	@ResponseBody
	public String getNonOpTy(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getNonOpTy(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getStockInfo")
	@ResponseBody
	public ChartVo getStockInfo(ChartVo chartVo){
		try {
			chartVo = chartService.getStockInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	@RequestMapping(value="getNonOpInfo")
	@ResponseBody
	public String getNonOpInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getNonOpInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="updateStock")
	@ResponseBody
	public String updateStock(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.updateStock(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="okDelStock")
	@ResponseBody
	public String okDelStock(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.okDelStock(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addStock")
	@ResponseBody
	public String addStock(String val){
		String str = "";
		try {
			str = chartService.addStock(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getMatInfo")
	@ResponseBody
	public String getMatInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getMatInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getPrdNo")
	@ResponseBody
	public String getPrdNo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getPrdNoList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="edit_svg")
	public String edit_svg(){
		return "chart/edit_svg";
	};
	
	@RequestMapping(value="updateBGImg")
	@ResponseBody
	public String updateBGImg(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.updateBGImg(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="addMachine")
	@ResponseBody
	public String addMachine(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addMachine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getLeadTime")
	@ResponseBody
	public String getLeadTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLeadTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="updateCd")
	@ResponseBody
	public String updateCd(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.updateCd(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str; 
	}
	
	@RequestMapping(value="addTarget")
	public String addTarget(){
		return "chart/addTarget";
	};
	
	@RequestMapping(value="okDel")
	@ResponseBody
	public String okDel(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.okDel(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getCnt")
	@ResponseBody
	public String getCnt(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getChecker")
	@ResponseBody
	public String getChecker(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getChecker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getFaultList")
	@ResponseBody
	public String getFaultList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getFaultList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="addTmpFaulty")
	@ResponseBody
	public String addFaulty(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addFaulty(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getDevieList")
	@ResponseBody
	public String getDevieList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getDevieList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getCheckTyList")
	@ResponseBody
	public String getCheckTyList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getCheckTyList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getPrdNoList")
	@ResponseBody
	public String getPrdNoList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getPrdNoList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getFaultyList")
	@ResponseBody
	public String getFaultyList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getFaultyList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="leadTime")
	public String leadTime(){
		return "chart/leadTime";
	}
	
	@RequestMapping(value="faulty")
	public String customerFaulty(){
		return "chart/faulty";
	}
	
	@RequestMapping(value="faulty_kpi")
	public String customerFaulty_kpi(){
		return "chart/faulty_kpi";
	}
	
	@RequestMapping(value="addFaulty")
	public String processFaulty(HttpServletRequest request){
		request.setAttribute("addFaulty", request.getParameter("addFaulty"));
		request.setAttribute("prdNo", request.getParameter("prdNo"));
		request.setAttribute("cnt", request.getParameter("cnt"));
		return "chart/addFaulty";
	}
	
	@RequestMapping(value="addFaultyHistory")
	public String addFaultyHistory(){
		return "chart/addFaultyHistory";
	}

	@RequestMapping(value="banner")
	public String banner(){
		return "chart/banner";
	}
	
	@RequestMapping(value="reOpCycle")
	public String reOpCycle(){
		return "chart/reOpCycle";
	}
	
	@RequestMapping(value="getWorkerList")
	@ResponseBody
	public String getWorkerList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getWorkerList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getAdtList")
	@ResponseBody
	public String getAdtList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAdtList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="adapterManager")
	public String adapterManager(){
		return "chart/adapterManager";
	};
	
	
	@RequestMapping(value="getTemplate")
	@ResponseBody
	public String getTemplate(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTemplate(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	@RequestMapping(value="getCurrentProg")
	@ResponseBody
	public ChartVo getCurrentProg(ChartVo chartVo){
		try {
			chartVo = chartService.getCurrentProg(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	
	@RequestMapping(value="programManager_tool")
	public String programManager_kpi(){
		return "chart/programManager_tool";
	};
	
	@RequestMapping(value="programManager")
	public String programManager(){
		return "chart/programManager";
	};
	
	@RequestMapping(value="getSLCR")
	@ResponseBody
	public String getSLCR(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getSLCR(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value="getPrgInfo")
	@ResponseBody
	public String getPrgInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getPrgInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value="getProgCd")
	@ResponseBody
	public String getProgCd(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getProgCd(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getPrgCdList")
	@ResponseBody
	public String getPrgCdList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getPrgCdList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getPerformanceAgainstGoal")
	@ResponseBody
	public String getPerformanceAgainstGoal(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getPerformanceAgainstGoal(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getGroup")
	@ResponseBody
	public String getGroup(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getGroup(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo){
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value="performanceAgainstGoal")
	public String performanceAgainstGoal(){
		return "chart/performanceAgainstGoal";
	};
	
	@RequestMapping(value="stockStatus")
	public String stockStatus(){
		return "chart/stockStatus";
	};
	
	@RequestMapping(value="addBanner")
	@ResponseBody
	public void addBanner(ChartVo chartVo){
		try {
			chartService.addBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="resetTool")
	@ResponseBody
	public void resetTool(ChartVo chartVo){
		try {
			chartService.resetTool(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getFacilitiesStatus")
	@ResponseBody
	public String getFacilitiesStatus(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getFacilitiesStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="prdStatus_MCT")
	public String neonsign_MCT(){
		return "chart/neonSign_MCT";
	}
	
	@RequestMapping(value="prdStatus_CNC")
	public String neonsign_CNC(){
		return "chart/neonSign_CNC";
	}
	
	@RequestMapping(value="performanceAgainstGoal_chart")
	public String performanceAgainstGoal_chart(){
		return "chart/performanceAgainstGoal_chart";
	}
	
	@RequestMapping(value="performanceAgainstGoal_chart_kpi")
	public String performanceAgainstGoal_chart_kpi(){
		return "chart/performanceAgainstGoal_chart_kpi";
	}
	
	@RequestMapping(value="tableChart")
	public String tableChart(){
		return "chart/tableChart";
	};
	
	@RequestMapping(value="traceManager")
	public String traceManager(){
		return "chart/traceManager";
	};
	
	@RequestMapping(value="lotTracer")
	public String lotTracer(){
		return "chart/lotTracer";
	};
	
	@RequestMapping(value="getLotTracer")
	@ResponseBody
	public String getLotTracer(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLotTracer(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getTraceHist")
	@ResponseBody
	public String getTraceHist(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTraceHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getData")
	@ResponseBody
	public ChartVo getData(ChartVo chartVo){
		try {
			chartVo = chartService.getData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo; 
	}
	
	@RequestMapping(value="getAllDvc")
	@ResponseBody
	public String getAllDvc(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAllDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getDvcId")
	@ResponseBody
	public String getDvcId(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}; 
	
	@RequestMapping(value="getReOpList")
	@ResponseBody
	public String getReOpList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getReOpList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getRepairList")
	@ResponseBody
	public String getRepairList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getRepairList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="	getDetailBlockData")
	@ResponseBody
	public String 	getDetailBlockData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.	getDetailBlockData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getDvcNameList")
	@ResponseBody
	public String getDvcNameList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getDvcNameList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getDvcIdForSign")
	@ResponseBody
	public String getDvcIdForSign(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getDvcIdForSign(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getTimeData")
	@ResponseBody
	public String getTimeData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTimeData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getTargetData")
	@ResponseBody
	public String getTargetData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTargetData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="addTargetCnt")
	@ResponseBody
	public String addTargetCnt(String val){
		String str = "";
		try {
			str = chartService.addTargetCnt(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getJigList")
	@ResponseBody
	public String getJicList(ChartVo chartVo){
		String str = null;
		try {
			str = chartService.getJicList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="rotateChart")
	public String rotateChart(ChartVo chartVo, HttpServletRequest request){
		request.setAttribute("fromDashBoard", request.getParameter("fromDashBoard"));
		return "chart/detailChart_block_rotate";
	};
	
	@RequestMapping(value="getToolList")
	@ResponseBody
	public String getToolList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getToolList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="toolManager")
	public String toolManager(ChartVo chartVo){
		return "chart/toolManager";
	};
	
	@RequestMapping(value="getAlarmList")
	@ResponseBody
	public String getAlarmList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAlarmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="dailyChart")
	public String dailyChart(ChartVo chartVo, HttpServletRequest request){
		request.setAttribute("dvcName", chartVo.getName());
		request.setAttribute("sDate", chartVo.getSDate());
		request.setAttribute("eDate", chartVo.getEDate());
		
		return "/chart/wcGraph";
	};
	
	@RequestMapping(value="getLatestDate")
	@ResponseBody
	public String getLatestDate(){
		String str = "";
		try {
			str = chartService.getLatestDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getJigList4Report")
	@ResponseBody
	public String getJigList4Report(ChartVo chartVo){
		String result = ""; 
		try {
			result = chartService.getJigList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getDvcList")
	@ResponseBody
	public String getDvcList(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getDvcList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWcData")
	@ResponseBody
	public String getWcData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getWcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWcDataByDvc")
	@ResponseBody
	public String getWcDataByDvc(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getWcDataByDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWorkerInfoo")
	@ResponseBody
	public ChartVo getWorkerInfoo(ChartVo chartVo){
		try {
			chartVo = chartService.getWorkerInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	@RequestMapping(value="addWorker")
	@ResponseBody
	public String addWorker(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.addWorker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getNextWorkerId")
	@ResponseBody
	public String getNextWorkerId(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getNextWorkerId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getAllWorkerList")
	@ResponseBody
	public String getAllWorkerList(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getAllWorkerList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getTableData")
	@ResponseBody
	public String getTableData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getTableData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWcList")
	@ResponseBody
	public String getWcList(ChartVo chartVo){
		String str = null;
		try {
			str = chartService.getWcList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="highChartTest")
	public String highChartTest(){
		return "chart/highChartTest";
	};
	
	@RequestMapping(value="getAlarmData")
	@ResponseBody
	public String getAlarmData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getAlarmData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="performanceReport")
	public String performanceReport(){
		return "chart/performanceReport";
	};
	
	@RequestMapping(value="jigGraph")
	public String jigGraph(HttpServletRequest request, ChartVo chartVo){
		request.setAttribute("group", chartVo.getGroup());
		return "chart/jigGraph";
	};
	
	@RequestMapping(value="jigGraph_kpi")
	public String jigGraph_kpi(HttpServletRequest request, ChartVo chartVo){
		request.setAttribute("group", chartVo.getGroup());
		return "chart/jigGraph_kpi";
	};
	
	@RequestMapping(value="wcGraph")
	public String wcGraph(){
		return "chart/wcGraph";
	};
	
	@RequestMapping(value="alarmReport")
	public String alarmReport(){
		return "chart/alarmReport";
	};
	
	@RequestMapping(value="DIMM")
	public String DIMM(){
		return "chart/DIMM";
	};
	
	@RequestMapping(value="dimf")
	public String dkimf(){
		return "chart/DIMF";
	};
	
	@RequestMapping(value="edit")
	public String edit(){
		return "chart/dashBoard_edit";
	};
	
	@RequestMapping(value="main_sb")
	public String main_sb(){
		return "chart/dashBoard_sb";
	};
	
	
	@RequestMapping(value="addPrdCmpl")
	public String addPrdCmpl(){
		return "chart/addPrdCmpl";
	}
	
	@RequestMapping(value="addPrdCmplHistory")
	public String addPrdCmplHistory(){
		return "chart/addPrdCmplHistory";
	};
	
	@RequestMapping(value="index")
	public String index() throws Exception{
		
		return "chart/index";
	}
	
	@RequestMapping(value="main")
	public String main(Model model) throws Exception{
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
		String ip = request.getHeader("X-Forwarded-For");
		 
		System.out.println(">>>> X-FORWARDED-FOR : " + ip);
 
        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
            System.out.println(">>>> Proxy-Client-IP : " + ip);
        }
        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP"); // 웹로직
            System.out.println(">>>> WL-Proxy-Client-IP : " + ip);
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
            System.out.println(">>>> HTTP_CLIENT_IP : " + ip);
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            System.out.println(">>>> HTTP_X_FORWARDED_FOR : " + ip);
        }
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        
        System.out.println(">>>> Result : IP Address : "+ip);
        
	    model.addAttribute("clientIP", ip);
	    return "chart/main";
	};
	
	@RequestMapping(value="checkPrdct")
	public String checkPrdct(){
		
		return "chart/checkPrdct";
	};
	
	@RequestMapping(value="checkPrdctStandard")
	public String checkPrdctStandard(){
		
		return "chart/checkPrdctStandard";
	};
	
//	@RequestMapping(value="main")
//	public String dashBoard(){
//		
//		return "chart/dashBoard";
//	};
	
	@RequestMapping(value="main_tmp")
	public String dashBoard5(){
		
		return "chart/dashBoard5";
	};

	@RequestMapping(value="main_test")
	public String main_test(){
		
		return "chart/dashBoard_test";
	};
	
	@RequestMapping(value="FC")
	public String FC(){
		
		return "chart/dashBoard_fc";
	};
	
	@RequestMapping(value="incomeStock")
	public String incomeStock(){
		return "chart/incomeStock";
	};
	
	@RequestMapping(value="workerMaanger")
	public String workerMaanger(){
		return "chart/workerManager";
	};
	
	@RequestMapping(value="incomeStockHistory")
	public String incomeStockHistory(){
		return "chart/incomeStockHistory";
	};
	
	@RequestMapping(value="releaseStock")
	public String release(){
		return "chart/release";
	};
	
	@RequestMapping(value="releaseStockHistory")
	public String releaseStockHistory(){
		return "chart/releaseHistory";
	}
	
	@RequestMapping(value="shipment")
	public String shipment(){
		return "chart/shipment";
	};
	
	@RequestMapping(value="shipmentHistory")
	public String shipmentHistory(){
		return "chart/shipmentHistory";
	};
	
	@RequestMapping(value="main_slide")
	public String main_slide(){
		return "chart/dashBoard_slide";
	};
	
	@RequestMapping(value="main2_slide")
	public String main2_slide(){
		return "chart/dashBoard2_slide";
	};
	
	@RequestMapping(value="main3_slide")
	public String main3_slide(){
		return "chart/dashBoard3_slide";
	};
	
	@RequestMapping(value="machine_status")
	public String machine_status(){
		return "chart/machine_status";
	};
	
	@RequestMapping(value="main4_slide")
	public String main4_slide(){
		return "chart/dashBoard4_slide";
	};
	
	@RequestMapping(value="main_en")
	public String main_en(){
		return "chart/dashBoard_en";
	};
	
	@RequestMapping(value="reSizing")
	public String reSizing(){
		return "chart/dashBoard_resizing";
	};
	
	@RequestMapping(value="multiVision")
	public String multiVision(){
		return "chart/multiVision";
	};
	
//	@RequestMapping(value="index")
//	public String index(){
//		return "chart/index";
//	};
	
	@RequestMapping(value="IE")
	public String IE(){
		return "chart/dashBoard_IE";
	};
	
	@RequestMapping(value="main2")
	public String main2(){
		return "chart/dashBoard2";
	};
	
	@RequestMapping(value="main3")
	public String main3(){
		return "chart/dashBoard3";
	};
	
	@RequestMapping(value="main4")
	public String main4(){
		return "chart/dashBoard4";
	};
	
	@RequestMapping(value="mobile")
	public String mobile(){
		return "chart/newMobile";
		//return "redirect:http://mtmes.doosanmachinetools.com/DULink/chart/mobile.do";
	};

	@RequestMapping(value="mobile2")
	public String mobile2(){
		return "chart/mobile2";
	};
	
	
	
	
	@RequestMapping(value="mobile3")
	public String mobile3(){
		return "chart/mobile3";
	};
	
	@RequestMapping(value="mobile_alarm")
	public String mobile_alarm(){
		return "chart/mobile_alarm";
	};
	@RequestMapping(value="mobile_screen")
	public String mobile_screen(){
		return "chart/mobile_screen";
	};
	@RequestMapping(value="mobile_account")
	public String mobile_account(){
		return "chart/mobile_account";
	};
	
	
	
	@RequestMapping(value="highChart1")
	public String highChart1(){
		return "chart/highChart1";
	};
	
	@RequestMapping(value="highChart2")
	public String highChart2(){
		return "chart/highChart2";
	}
	
	@RequestMapping(value="highChart3")
	public String highChart3(){
		return "chart/highChart3";
	};
	
	@RequestMapping(value="highChart4")
	public String highChart4(){
		return "chart/highChart4";
	};
	
	@RequestMapping(value="video")
	public String video(){
		return "test/video";
	}
	
	@RequestMapping(value="video2")
	public String video2(){
		return "test/video2";
	};
	
	@RequestMapping(value="getStatusData")
	@ResponseBody
	public String getStatusData(ChartVo chartVo){
		String result = "";
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			result = chartService.getStatusData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//result = chartVo.getCallback() + "(" + result + ")";
		return result;
	};
	
	@RequestMapping(value="test")
	@ResponseBody
	public void test(){
		ChartVo chartVo = new ChartVo();
		chartVo.setDate("2015-05-09");
		try {
			chartService.test(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping(value="getCurrentDvcData")
	@ResponseBody
	public ChartVo getCurrentDvcData(ChartVo chartVo){
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			chartVo = chartService.getCurrentDvcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return chartVo;
	};
	
	//@Scheduled(fixedDelay = 5000)
	public void addData(){
		try {
			chartService.addData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getDvcName")
	@ResponseBody
	public String getDvcName(ChartVo chartVo){
		String dvcName = null;
		try {
			dvcName = chartService.getDvcName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dvcName;
	};
	
	@RequestMapping(value="getAllDvcStatus")
	@ResponseBody
	public String getAllDvcStatus(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getAllDvcStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getAllDvcId")
	@ResponseBody
	public String getAllDvcId(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getAllDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	
	@RequestMapping(value="getBarChartDvcId")
	@ResponseBody
	public String getBarChartDvcId2(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getBarChartDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	public String addZero(String str){
		String rtn = str;
		if(rtn.length()==1){
			rtn = "0" + str;
		};
		return rtn;
	};
	
	@RequestMapping(value="getTime")
	@ResponseBody
	public String getTime(){
		String rtn = "";

		Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
		//cal.add ( cal.DATE, 1); //2개월 전....
		
		String year = String.valueOf(cal.get ( cal.YEAR ));
		String month = String.valueOf(cal.get ( cal.MONTH )+1);
		String day = String.valueOf(cal.get ( cal.DATE ));
		
		String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
		String minute = String.valueOf(cal.get(cal.MINUTE));
		String second = String.valueOf(cal.get(cal.SECOND));
		rtn = year + ". " + month + ". " + day + ".-" + hour + ":" + minute + ":" + second;
		return rtn;
	};
	
	@RequestMapping(value="dashBoard_pad")
	public String pad(){
		return "chart/dashBoard_pad";
	};
	
	@RequestMapping(value="dashBoard_left")
	public String pad_left(){
		return "chart/dashBoard_left";
	};
	
	@RequestMapping(value="dashBoard_right")
	public String pad_right(){
		return "chart/dashBoard_right";
	};
	
	@RequestMapping(value="dashBoard_center")
	public String center(){
		return "chart/dashBoard_center";
	};
	
	@RequestMapping(value="polling1")
	@ResponseBody
	public String polling1() throws Exception{
		return chartService.polling1();
	};
	
	@RequestMapping(value="setVideo")
	@ResponseBody
	public String setVideo(String id) throws Exception{
		return chartService.setVideo(id);
	};
	
	@RequestMapping(value="initVideoPolling")
	@ResponseBody
	public void initVideoPolling() throws Exception{
		chartService.initVideoPolling();
	}
	
	@RequestMapping(value="initPiePolling")
	@ResponseBody
	public void initPiePolling() throws Exception{
		chartService.initPiePolling();
	};
	
	@RequestMapping(value="piePolling1")
	@ResponseBody
	public String piePolling1() throws Exception{
		return chartService.piePolling1();
	};
	
	@RequestMapping(value="piePolling2")
	@ResponseBody
	public String piePolling2() throws Exception{
		return chartService.piePolling2();
	};
	
	@RequestMapping(value="setChart1")
	@ResponseBody
	public String setChart1(String id) throws Exception{
		return chartService.setChart1(id);
	};
	
	@RequestMapping(value="setChart2")
	@ResponseBody
	public String setChart2(String id) throws Exception{
		return chartService.setChart2(id);
	};
	
	@RequestMapping(value="delVideoMachine")
	@ResponseBody
	public void delVideoMachine(String id) throws Exception{
		chartService.delVideoMachine(id);
	};
	
	@RequestMapping(value="delChartMachine")
	@ResponseBody
	public void delChartMachine(String id) throws Exception{
		chartService.delChartMachine(id);
	};
	
	@RequestMapping(value="getAdapterId")
	@ResponseBody
	public String getAdapterId(ChartVo chartVo){
		String result = null;
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			//result = chartService.getAllDvcId(chartVo);
			result = chartService.getAdapterId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="showDetailEvent")
	@ResponseBody
	public String showDetailEvent(ChartVo chartVo) throws Exception{
		return chartService.showDetailEvent(chartVo);
	};
	
	@RequestMapping(value="getDetailStatus")
	@ResponseBody
	public String getDetailStatus(ChartVo chartVo)throws Exception{
		String result = null;
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(chartVo.getStartDateTime());
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			
			chartVo.setDate(year + "-" + month + "-" + date);
			chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + hour + ":00");
			chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + hour + ":59");
			
			if(Integer.parseInt(hour)>=20 && Integer.parseInt(hour)!=24){
				cal.add ( cal.DATE, -1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + hour + ":00");
				chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + hour + ":59");
			}else if(Integer.parseInt(hour)==24){
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + "00:00");
				chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + "00:59");
			};
			
			//result = chartService.getAllDvcId(chartVo);
			result = chartService.getDetailStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	
	// need param
	// -- deprecated. : inputVo.date = workdate
	// inputVo.dvcId = dvcId
	// inputVo.targetDateTime = yyyy-MM-dd HH:mm:ss
	@RequestMapping(value="getTimeChart")
	@ResponseBody
	public List<BarChartVo> getTimeChart(ChartVo chartVo){
		String workDate = CommonFunction.dateTime2WorkDate(chartVo.getTargetDateTime());
		chartVo.setDate(workDate);
		
		List<BarChartVo> chart = new ArrayList<BarChartVo>();
		List<TimeChartVo> listTime =  chartService.getTimeChartData(chartVo);
		
		//rtn Data is empty.
		if(listTime == null){
			return null;
		}
		
		Iterator<TimeChartVo> ite = listTime.iterator();
		while(ite.hasNext()){
			TimeChartVo srcVo = ite.next();
			BarChartVo tmpVo = new BarChartVo();
			List<DataVo> data = new ArrayList<DataVo>();
			DataVo tmpDataVo = new DataVo();
			tmpDataVo.setStartTime(srcVo.getStartTime());
			tmpDataVo.setEndTime(srcVo.getEndTime());
			tmpDataVo.setY(srcVo.getY());
			data.add(tmpDataVo);
			tmpVo.setData(data);
			tmpVo.setColor(srcVo.getColor());
			chart.add(tmpVo);
		}
		return chart;
	};
	

	@RequestMapping(value="getOtherChartsData")
	@ResponseBody
	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception{
		return chartService.getOtherChartsData(chartVo);
	};
	
	@RequestMapping(value="testTimeChart")
	@ResponseBody
	public List<ChartVo> testTimeChart(ChartVo chartVo) throws Exception{
		return chartService.testTimeChartData(chartVo);
	};

	@RequestMapping(value="singleChartStatus")
	public String singleChartStatus(HttpServletRequest request, HttpServletResponse response){
		request.setAttribute("fromDashBoard", request.getParameter("fromDashBoard"));
		return "chart/singleChartStatus";
		//return "chart/detailChart_block";
	};
	
	@RequestMapping(value="singleChartStatus2")
	public String singleChartStatus2(){
		return "chart/singleChartStatus";
	};
	
	@RequestMapping(value="singleChartStatus_MCT")
	public String singleChartStatus_MCT(){
		return "chart/singleChartStatus_MCT";
	};
	
	@RequestMapping(value="singleChartStatus_CNC")
	public String singleChartStatus_CNC(){
		return "chart/singleChartStatus_CNC";
	};
	
	@RequestMapping(value="prdStatus")
	public String neonsign(HttpServletRequest request){
		String line = request.getParameter("line");
		
		request.setAttribute("line", line);
		return "chart/neonSign";
	}
	
	@RequestMapping(value="setLimit")
	@ResponseBody
	public void setLimit(ChartVo chartVo){
		try {
			chartService.setLimit(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="delMachine")
	@ResponseBody
	public String delMachine(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.delMachine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getBGImg")
	@ResponseBody
	public String getBGImg(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getBGImg(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="setMachinePos_edit")
	@ResponseBody
	public void setMachinePos(ChartVo chartVo){
		try {
			chartService.setMachinePos_edit(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getMachine")
	@ResponseBody
	public String getMachine(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getMachine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getRcvInfo")
	@ResponseBody
	public String getRcvInfo(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getRcvInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getMachineName")
	@ResponseBody
	public String getMachineName(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getMachineName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="checkId")
	@ResponseBody
	public boolean checkId(ChartVo chartVo){
		boolean check = false;
		try {
			check = chartService.checkId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;
	}
	
	@RequestMapping(value="doSignIn")
	@ResponseBody
	public boolean doSignIn(ChartVo chartVo){
		boolean check = false;
		try {
			check = chartService.doSignIn(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;
	}
	
	@RequestMapping(value="getComNameList")
	@ResponseBody
	public String getComNameList(){
		String result="failed";
		try {
			result = chartService.getComNameList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="getUser")
	@ResponseBody
	public String getUser(UserVO userVO){
		String result="failed";
		try {
			result = chartService.getUser(userVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	

	@RequestMapping(value="setUserInfo")
	@ResponseBody
	public String setUserInfo(UserVO userVO){
		String result="failed";
		try {
			result = chartService.setUsernameAndPassword(userVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="setUser")
	@ResponseBody
	public String setUser(UserVO userVO){
		String result="failed";
		try {
			result = chartService.setUser(userVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="getCheckedList")
	@ResponseBody
	public String getCheckedList(ChartVo chartVo){
		String result="failed";
		try {
			result = chartService.getCheckedList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="pushSave")
	@ResponseBody
	public String pushSave(ChartVo chartVo){
		String result="failed";
		try {
			result = chartService.pushSave(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="logout")
	@ResponseBody
	public String logout(ChartVo chartVo){
		String result="failed";
		try {
			result = chartService.logout(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="getStatus")
	@ResponseBody
	public String getStatus(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
};

