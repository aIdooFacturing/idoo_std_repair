<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>iDOO Control</title>
<style type="text/css">
body{
	background-color : black;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">


<script src="${ctxPath }/js/request_repair.js"></script>
	

</head>
<body>	
	<div id="container">
		<center>
			<div id="button_div">
				<button onclick="addRow()">추가</button>
				<button onclick="saveRow()">저장</button>
			</div>
			
			<div id="tableDiv">
				<table id="mainTable">
				<thead>
					<tr>
						<th style="width : 20%">JIG</th>
						<th style="width : 20%">장비</th>
						<th style="width : 50%">내용</th>
						<th style="width : 10%"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>	
			</div>
			
		</center>
	</div>	
</body>
</html>	