const loadPage = () =>{
	createMenuTree("maintenance", "RepairHst")
	
	setEl()
	getJig()
	getGroup("all");
	
	$("#eDate").val(getToday().substr(0,10).replace(/\./gi, "-"))
	$("#sDate").val(getSubtractDate(6))
}
	
const getSubtractDate = (n) => {
    let date = new Date()
    date.setDate(date.getDate() - n)

    return date.getFullYear() + "-" + addZero(String(date.getMonth() + 1)) + "-" + addZero(String(date.getDate()))
}

const setEl = () =>{
	
	
	$("#button_div").css({
		"width" : getElSize(1888 * 2),
		"height" : getElSize(64 * 2),
		"background" : "#2B2D32",
		"box-shadow" : "0 " + getElSize(2 * 2) + "px " + getElSize(4 * 2) + "px 0 rgba(0,0,0,0.50)",
		"border-radius" : getElSize(4 * 2) + "px",
		"margin-top" : getElSize(16 * 2)
	});
	
	$("#tableDiv").css({
		"margin-top" : getElSize(16 * 2),
		"padding-top" : getElSize(16 * 2),
		"padding-bottom" : getElSize(16 * 2),
		"width" : getElSize(1888 * 2),
		"background-color" : "#2B2D32",
		"height" : getElSize(1550),
		"overflow" : "auto"
	})
	
	$("table").css({
		"width" : getElSize((1888 - 32) * 2),
		
	})
	$("thead th").css({
		"color" : "white",
		"font-size" : getElSize(18 * 2),
		"background-color" : "#353542 100%",
		"padding" : getElSize(10 * 2),
		
	})
	
	$("button").css({
		"height" : getElSize(40 * 2),
		"font-size" : getElSize(24 * 2),
		"background" : "#9B9B9B",
		"border-radius": getElSize(2 * 2) + "px",
		"border":"none",
		"float" : "left",
		"margin-left" : getElSize(12 * 2) + "px",
		"margin-top" : getElSize(12 * 2) + "px",
		"cursor" : "pointer"
	}).click(getRepairHst)
	
	$("input[type='date']").css({
		"height" : getElSize(40 * 2),
		"width" : getElSize(600),
		"font-size" : getElSize(24 * 2),
		"border-radius": getElSize(2 * 2) + "px",
		"border":"none",
		"float" : "left",
		"margin-left" : getElSize(12 * 2) + "px",
		"margin-top" : getElSize(12 * 2) + "px",
		"padding-left" : getElSize(22 * 2) + "px",
		"cursor" : "pointer"
	})
	
	
	$("select").css({
		"width" : `${getElSize(400)}px`, 
		"padding-left" : `${getElSize(8 * 2)}`,
		"float" : "left",
		"margin-left" : getElSize(12 * 2) + "px",
		"margin-top" : getElSize(12 * 2) + "px",
	})
}

const getRepairHst = () =>{
	const url = ctxPath + "/getRepairHst.do";
	const param = "dvcId=" + $("#group").val()
				+ "&sDate=" + $("#sDate").val()
				+ "&eDate=" + $("#eDate").val() + " 23:59:59"
				+ "&shopId=" + shopId
				+ "&jig=" + $("#jig").val();
	
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
		success : (data)=>{
			let tr = ""
			for(json of data.statusList){
				tr += 
					`
						<Tr>
							<Td>${decodeURIComponent(json.jig)}</td>
							<Td>${decodeURIComponent(json.name)}</td>
							<Td>${decodeURIComponent(json.content).replace(/\+/gi, " ")}</td>
							<Td>${toLocaleTime(json.date.substr(0,19))}</td>
						</tr>
					`
			}
			
			if(data.statusList.length == 0){
				tr += 
					`
						<Tr>
							<Td colspan="4">수리 이력이 없습니다.</td>
						</tr>
					`
			}
			
			$("tbody").html(tr).css({
				"color" : "color",
				"text-align" : "center"
			});
			
			$("tbody td").css({
				"font-size" : getElSize(18 * 2),
				"padding" : getElSize(10 * 2)
			})
			
			$("tbody tr:odd").css({
				"background-color" : "#DCDCDC",
				"opacity" : "0.9"	
			})
			
			$("tbody tr:even").css({
				"background-color" : "#F0F0F0",
				"opacity" : "0.9"
			})
		}
	})
}

const toLocaleTime = (time) =>{
	const date = new Date(time + ' UTC');
	const year = date.getFullYear();
	const month = addZero(String(date.getMonth() + 1))
    const day = addZero(String(date.getDate()))

    const hour = addZero(String(date.getHours()))
    const minute = addZero(String(date.getMinutes()))
    const second = addZero(String(date.getSeconds()))

    return year + "-" + month + "-" + day + " " + hour + " : " + minute + " : " + second
}

const getJig = () => {
	const url = ctxPath + "/getJigList4Report.do";
	const param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			let json = data.jigList;
			
			
			
			let option = "<option value='all'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.jig + "'>" + decodeURIComponent(data.jig).replace(/\+/gi," ") + "</option>";  
			});
			
			$("#jig").html(option).change(()=>{
				getGroup($("#jig").val())
			});
		}
	});
};

const getGroup = (jig) =>{
	var url = ctxPath + "/getDeviceList4Report.do";
	var param = "shopId=" + shopId;
	
	if (jig != ""){
		param += "&jig="+jig;
	}
	
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			let json = data.dvcList;
			
			let option = "<option value='all'>전체</option>";
			
			$(json).each(function(idx, data){
					
				option += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";  

			});
			
			$("#group").html(option);
			
		//	getAlarmData();
		}
	});
};
