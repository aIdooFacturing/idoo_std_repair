const loadPage = () =>{
	createMenuTree("maintenance", "request_Repair")
	
	setEl()
}
	
const setEl = () =>{
	
	
	$("#button_div").css({
		"width" : getElSize(1888 * 2),
		"height" : getElSize(64 * 2),
		"background" : "#2B2D32",
		"box-shadow" : "0 " + getElSize(2 * 2) + "px " + getElSize(4 * 2) + "px 0 rgba(0,0,0,0.50)",
		"border-radius" : getElSize(4 * 2) + "px",
		"margin-top" : getElSize(16 * 2)
	});
	
	$("#tableDiv").css({
		"margin-top" : getElSize(16 * 2),
		"padding-top" : getElSize(16 * 2),
		"padding-bottom" : getElSize(16 * 2),
		"width" : getElSize(1888 * 2),
		"background-color" : "#2B2D32",
		"height" : getElSize(1550),
		"overflow" : "auto"
	})
	
	$("table").css({
		"width" : getElSize((1888 - 32) * 2),
		
	})
	$("thead th").css({
		"color" : "white",
		"font-size" : getElSize(18 * 2),
		"background-color" : "#353542 100%",
		"padding" : getElSize(10 * 2),
		
	})
	
	$("button").css({
		"height" : getElSize(40 * 2),
		"font-size" : getElSize(24 * 2),
		"background" : "#9B9B9B",
		"border-radius": getElSize(2 * 2) + "px",
		"border":"none",
		"float" : "left",
		"margin-left" : getElSize(12 * 2) + "px",
		"margin-top" : getElSize(12 * 2) + "px",
		"cursor" : "pointer"
	})
}

const getGroup = () =>{
	
	var url = ctxPath + "/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	let select;
		
	$.ajax({
		url : url,
		data : param,
		type : "post",
		async : false,
		dataType : "json",
		success : function(data){
			let json = data.dataList;
			
			select = 
				`
					<select onchange="changeJig(this)" class="jig" style="width : ${getElSize(400)}px; padding-left : ${getElSize(8 * 2)}">
						<option value='all'>선택</option>
				`
				
			$(json).each(function(idx, data){
				select += "<option value='" + data.prdNo + "'>" + decodeURIComponent(data.prdNo).replace(/\+/gi," ") + "</option>";  
			});
			
			select += "</select>"

		}
	});
	

	return 	select
}

const changeJig = (el) =>{
	let jig = $(el).val()
	
	let dvcSelect = $(el).parent("td").next("td").children("select")
	

	dvcSelect.html(getDeviceList(jig))
}

const getDeviceList = (jig = "all") =>{
	let select; 
		
	var url = ctxPath + "/getDeviceList4Report.do";
	var param = "shopId=" + shopId
			+ "&jig=" + jig;
	
	$.ajax({
		url : url,
		async : false,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			let json = data.dvcList;
			
			
			
			select = 
				`
					<select class="dvcId" style="width : ${getElSize(400)}px; padding-left : ${getElSize(8 * 2)}">
						<option value='all'>선택</option>
				`
				
			
			$(json).each(function(idx, data){
					
				select += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";  

			});	
			
			select += `</select>`
		}
	});
	
	return 	select
};

const getInputText = () =>{
	const input = 
		`
			<input type="text" 
				class="content"
				style=
					"
						font-size : ${getElSize(24 * 2)}px;
						color : white;
						background-color : black;
						padding : ${getElSize(10 * 2)}px;
						border : none;
						outline : none;
						width : 90%;
					"
				
			>
		`
		
	return input;
}


const saveRow = () =>{
	for(let tr of $("tbody tr")){
		let jig = $(tr).find(".jig").val()
		let dvcId = $(tr).find(".dvcId").val()
		let content = $(tr).find(".content").val()
		
		const url = ctxPath + "/addRequestRepair.do";
		const param = "shopId=" + shopId
					+ "&dvcId=" + dvcId
					+ "&content=" + encodeURIComponent(content)
					+ "&userId=" + getCookie("user_id");
		
		
		console.log(param)
		$.ajax({
			url : url,
			async: false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){

			}
		});
	}
	
	alert("수리 요청이 완료 돠었습니다.")
	$("tbody tr").remove()
};

const getDelBtn = () =>{
	const btn = 
		`
			<button type="text"
				onclick="delRow(this)"
				style=
					"
						height : ${getElSize(40 * 2)}px;
						font-size : ${getElSize(24 * 2)}px;
						background : #9B9B9B;
						border-radius : ${getElSize(2 * 2)}px;
						border :none;
						cursor" : pointer;
					"
				
			>삭제</button>
		`
		
	return btn;
}

let test;
const delRow = (el) =>{
	test = $(el).parent("td").parent("tr")

	$(el).parent("td").parent("tr").remove()
}

const addRow = () =>{
	let bgColor = "";
		
	if($("table tbody tr").length % 2 == 0){
		bgColor = "#DCDCDC";
	}else{
		bgColor = "#F0F0F0";
	}
	
	const tr = 
		`
			<tr>
				<td style="background-color:${bgColor}; width : 20%; text-align : center; opacity : 0.9; padding : ${getElSize(10 * 2)}; font-size : ${getElSize(18 * 2)}px">${getGroup()}</td> 
				<td style="background-color:${bgColor}; width : 20%; text-align : center; opacity : 0.9; padding : ${getElSize(10 * 2)}; font-size : ${getElSize(18 * 2)}px">${getDeviceList()}</td>
				<td style="background-color:${bgColor}; width : 50%; text-align : center; opacity : 0.9; padding : ${getElSize(10 * 2)}; font-size : ${getElSize(18 * 2)}px">${getInputText()}</td>
				<td style="background-color:${bgColor}; width : 10%; text-align : center; opacity : 0.9; padding : ${getElSize(10 * 2)}; font-size : ${getElSize(18 * 2)}px">${getDelBtn()}</td>
			</tr>
		`
	$("table tbody").append(tr)	
}